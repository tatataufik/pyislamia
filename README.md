# Installation and Usage Examples
PyIslamia is python project as an initiative to calculate various aspects needed by a moslem based on astronomical calculation. 

# Requirements
 1. Python version 2.7.3 or later
 2. astronomia version 0.2.3 or later
 3. numpy version 1.8.0 or later

# Dependancy Installation (in Ubuntu)
Make sure you have python-pip installed.

    sudo apt-get install python-pip

To install the dependancy is as follow.

 1. Installation of astronomia.

        sudo pip install astronomia
        
 2. Installation of numpy.
  
        sudo pip install numpy
        
# Installation Steps
 1. Clone the repository. 
     
        git clone https://bitbucket.org/tatataufik/pyislamia.git
     
 2. Go to pyislamia folder. 
 
        cd pyislamia
        
 3. Install the module. 
 
        sudo python setup.py install
   
# API Reference
For complete reference, the API reference document can be downloaded in this [link](https://www.dropbox.com/s/ey8ilvwo238gt6c/refman.pdf).   

# Usage Examples
You can find more complete list of usage examples in [pyislamia-examples](http://bitbucket.org/tatataufik/pyislamia-examples/overview).

## Date and Time Operation
Create date_time_operation.py to have the following content.

~~~~~~~~~~~~~~~~~~~~~~~~{python}
from datetime import datetime
from pytz import timezone
from astronomia import calendar

def main():
	""" 
	Main Function
	"""

    '''timezone'''
    tz = timezone("Asia/Jakarta")
    '''today datetime'''
    dt = datetime.today()
    '''convert datetime to julian day'''
    jd = calendar.cal_to_jde(dt.year, dt.month, dt.day, dt.hour, dt.minute,dt.second)
    
    ''' date format for displaying yyyy-mm-dd HH:MM:SS, \
        e.g. 2014-03-12 20:05:30 WIB'''
    dateformat="%Y-%m-%d %H:%M:%S %Z"
    
    print "Datetime (Now)\t\t\t: %s" % dt.strftime(dateformat)
    print "in Julian Day\t\t\t: %f" % jd
    
    '''initialize datetime to a desired date and time, i.e. 2014-03-05 00:00:00'''
    dt = datetime(2014,3,5,0,0,0)
    '''convert datetime ke julian day'''
    jd = calendar.cal_to_jde(dt.year, dt.month, dt.day, dt.hour, dt.minute,dt.second)
    
    '''date format for displaying yyyy-mm-dd HH:MM:SS, e.g. 2014-03-12 20:05:30'''
    print "Datetime\t\t\t: %s" % dt.strftime(dateformat)
    print "in Julian Day\t\t\t: %f" % jd
    
    '''print date time with timezone info'''
    print "Datetime with Asia/Jakarta Tz\t: %s " % tz.localize(dt).strftime(dateformat)
    
    '''initialize timezone UTC'''
    tz_utc = timezone("UTC")
    
    '''convert Western Indonesia Time (WIB) to UTC time'''
    utc_dt = dt - tz.utcoffset(dt)
    print "Datetime in UTC\t\t\t: %s" % tz_utc.localize(utc_dt).strftime(dateformat)
    
    '''Convert UTC to Central Indonesia Time (WITA)'''
    tz_wita = timezone("Asia/Makassar")
    wita_dt = utc_dt + tz_wita.utcoffset(dt)
    print "Datetime in Asia/Makassar Tz\t: %s" % tz_wita.localize(wita_dt).strftime(dateformat)
    
        
if __name__ == '__main__':
    main()
      
~~~~~~~~~~~~~~~~~~~~~~~~

The following show the output when the script is running.

~~~~~~~~~~~~~~~~~~~~~~~~

$ python date_time_operation.py

Datetime (Now)					: 2014-03-14 15:54:33 
in Julian Day					: 2456731.162882
Datetime						: 2014-03-05 00:00:00 
in Julian Day					: 2456721.500000
Datetime with Asia/Jakarta Tz	: 2014-03-05 00:00:00 WIB 
Datetime in UTC					: 2014-03-04 17:00:00 UTC
Datetime in Asia/Makassar Tz	: 2014-03-05 01:00:00 WITA

~~~~~~~~~~~~~~~~~~~~~~~~

## Moon Ecliptical Position Calculation
Create script moon_position.py to have the following content.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{python}

from datetime import datetime
from pytz import timezone
from astronomia import calendar,util
from pyislamia.tool import frac_of_day
from pyislamia.elp82b import elp82b_coordinates
from astronomia import lunar
 

def main():
    """ 
	Main Function
	"""
	
    '''timezone'''
    tz = timezone("Asia/Jakarta")
    '''today datetime'''
    dt = datetime.today()
    jd = calendar.cal_to_jde(dt.year, dt.month, dt.day, dt.hour, dt.minute,dt.second)
    utc_jd = jd - frac_of_day(tz.utcoffset(dt)) #julian_day UTC
  
    """date time format"""
    fmt = '%Y-%m-%d'
    hour_fmt ='%H:%M:%S %Z'
    full_fmt = fmt + " " + hour_fmt
    
    """today datetime"""
    print "Date and Time\t\t: %s" %  tz.localize(dt).strftime(full_fmt)
    
    '''Calculation of moon ecliptical position using ELP 2000-82b'''
    ecl_pos = elp82b_coordinates(utc_jd)
    ecl_pos_in_deg = map(lambda x: util.r_to_d(x), ecl_pos[:2]) \
        + [ecl_pos[2],]
    print u"Ecl Moon Position from ELP 2000-82b: [%f\xb0 %f\xb0 %f] " % (ecl_pos_in_deg[0], ecl_pos_in_deg[1], ecl_pos_in_deg[2])
    
    
    '''Calculation of moon ecliptical position using ELP 2000 (Meeus)'''
    _lunar = lunar.Lunar()
    ecl_pos = _lunar.dimension3(utc_jd)
    ecl_pos_in_deg = map(lambda x: util.r_to_d(x), ecl_pos[:2]) + [ecl_pos[2],]
    print "Ecl Position from ELP 2000 (Meeus):  [%f\xb0 %f\xb0 %f] "  % (ecl_pos_in_deg[0], ecl_pos_in_deg[1], ecl_pos_in_deg[2])
        
if __name__ == '__main__':
    main()

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following show the output when the script is running.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
$ python moon_position.py
Date and Time		: 2014-03-14 16:01:41 WIB
Ecl Moon Position from ELP 2000-82b: [147.133358° -4.547963° 402228.411914] 
Ecl Position from ELP 2000 (Meeus):  [147.337239° -4.550547° 402229.636658] 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


"""@package pyislamia.parallax
Calculates the geocentric parallax in the equatorial coordinates of a celestial body
"""


import numpy as np
import constants as const
from tool import sincos


def geocentric_parallax(hr_ang, decl, distance,geog_lat,height_msl):
    """
    Calculates the geocentric parallax in the equatorial coordinates of a 
    celestial body.
     
    @param hr_ang The body's local hour angle in radians.
    @param decl The body's declination in radians.
    @param distance The body's distance from the Earth in AU. 
    @param geog_lat The observer's geographic latitude in radians.
    @param height_msl The observer's elevation/height above mean sea level in meters.
    @return [ The parallax in right ascension expressed in radians, The parallax in declination expressed in radians]
    """
 
   
    [gclat, gcrad] = _earth_figure_values(geog_lat, height_msl) #Get the observer's earth figure numbers

    """The horizontal parallax of the body"""
    hpx = np.sin(8.794 * const.ACS_TO_RAD) / distance;

    [sh,ch] = sincos(hr_ang)
    [sd,cd] = sincos(decl)
    [sl,cl] = sincos(gclat)
   
    '''correction for right ascension (RA)'''
    d_ra = np.arctan2(-gcrad * cl * hpx * sh, cd - gcrad * cl * hpx * ch)
    cd_ra = np.cos(d_ra)
    '''correction for declination'''
    d_dec  = np.arctan2((sd - gcrad * sl * hpx) * cd_ra, cd - gcrad * cl * hpx * ch) - decl

    return [d_ra, d_dec]

"""
 * Calculates various values related to the oblateness of the Earth.
 *
 * geog_lat -- The observer's geographic latitude in radians.
 * height_msl -- The observer's height above mean sea level in meters.
 * gc_lat -- The observer's geocentric latitude in radians.
 * gc_radius -- The observer's geocentric radius as a fraction of
 *              the Earth's equatorial radius.
 """
def _earth_figure_values(geog_lat, height_msl):

    sg = np.sin(geog_lat)
    cg = np.cos(geog_lat)
    x = np.arctan2(const.EARTH_POL_RADIUS * sg, const.EARTH_EQU_RADIUS * cg)
    s = np.sin(x)
    c = np.cos(x)
    
    c = c + (height_msl / const.EARTH_EQU_RADIUS) * cg
    s = (1.0 - const.EARTH_FLATTENING) * s + (height_msl / const.EARTH_EQU_RADIUS) * sg

    return [np.arctan2(s, c),np.sqrt(c * c + s * s)]





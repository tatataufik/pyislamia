"""@package pyislamia.elp82b
Calculate ecliptical position of the moon based on ELP 2000-82b parameters
"""

from elp82b_data import elp_terms
from astronomia import util,lunar
import numpy as np
from tool import to_julian_centuries
from constants import ACS_TO_RAD
from astroOps import fundamental_argument



def elp82b_coordinates(jd):
    """
    Calculate ecliptical position of the moon based on ELP 2000-82b parameters.
    @param jd julian day (UTC) 
    @return ecliptical poisition of the moon
    """
    
    
    t = to_julian_centuries(jd)
 
    ''' Constants needed for additive corrections to A in the main problem '''
    m = 129597742.2758 / 1732559343.73604;
    alpha2_3m = 0.00514376267 / (3.0 * m);
    d_nu = (-0.06424 - m * 0.55604) / 1732559343.73604;
    dnu2_3nu = 1.11208 / 5197678031.20812;
 
    """
    The following constants, fitted to DE200/LE200, are used to correct A
    in the main problem. b2, b3 and b4 in elp82b_data.c have already been
    multiplied with these values.
    del_e (0".01789), del_eprime (-0".12879), del_g (-0".08066)
    """
  
    """
    Delaunay arguments. These expressions are different from the ones used
    in the IAU precession model.
    """
    d  = (1072260.73512 +
        (1602961601.4603 +
        (-5.8681 +
        (0.006595 - 0.00003184 * t) * t) * t) * t) * ACS_TO_RAD;
 
    lp = (1287104.79306 +
        (129596581.0474 +
        (-0.5529 + 0.000147 * t) * t) * t) * ACS_TO_RAD;
 
    l  = (485868.28096 +
        (1717915923.4728 +
        (32.3893 +
        (0.051651 - 0.00024470 * t) * t) * t) * t) * ACS_TO_RAD;
 
    f  = (335779.55755 +
        (1739527263.0983 +
        (-12.2505 +
        (-0.001021 + 0.00000417 * t) * t) * t) * t) * ACS_TO_RAD;
 
    """
     * Longitudes of the planets, used in calculating perturbations. They
     * are different from the ones used in the IAU precession model.
    """
    me = (908103.25986 + 538101628.68898 * t) * ACS_TO_RAD;
    ve = (655127.28305 + 210664136.43355 * t) * ACS_TO_RAD;
    ma = (1279559.78866 + 68905077.59284 * t) * ACS_TO_RAD;
    ju = (123665.34212 + 10925660.42861 * t) * ACS_TO_RAD;
    sa = (180278.89694 + 4399609.65932 * t) * ACS_TO_RAD;
    ur = (1130598.01841 + 1542481.19393 * t) * ACS_TO_RAD;
    ne = (1095655.19575 + 786550.32074 * t) * ACS_TO_RAD;
 
    """
     * Mean longitude of the earth. A different expression from the argument
     * ARG_LONGITUDE_EARTH used in the IAU precession model.
    """
    T = (361679.22059 +
        (129597742.2758 +
        (-0.0202 +
        (0.000009 + 0.00000015 * t) * t) * t) * t) * ACS_TO_RAD;
 
    """ Mean longitude of the moon"""
    w = fundamental_argument("ARG_LONGITUDE_MOON", t);
 
    """
     * Angle of mean ecliptic of date wrt equinox of J2000, corrected with
     * the precessional constant.
    """
    zeta = (785939.95571 + 1732564372.83264 * t) * ACS_TO_RAD;

    lbr1 = [0.0 for i in range(0,3)]
    lbr2 = [0.0 for i in range(0,3)]
    lbr3 = [0.0 for i in range(0,3)]
    
    
    """ Main problem, files elp1 to elp3"""
    for i in range(1,4):
        k = i - 1;
        
        p1 = elp_terms[i];
        for j in range(len(elp_terms[i])-1,-1,-1):
            x = float(p1[j][0]) * d + float(p1[j][1])* lp + p1[j][2] * l + p1[j][3] * f
            y = p1[j][4] + (p1[j][5] + p1[j][9] * alpha2_3m) * d_nu + \
                p1[j][6] + p1[j][7] + p1[j][8]
            if (i != 3):
                lbr1[k] += y * np.sin(x)
            else:
                y -= p1[j][4] * dnu2_3nu
                lbr1[k] += y * np.cos(x)

     
    """ 
     * Perturbations: Earth (elp4-elp9), tidal effects (elp22-elp27),
     * Moon (elp28-elp30), relativistic (elp31-elp33), planetary effects
     * on solar eccentricity (elp34-elp36).
    """
    for i in range(4,37):
        if i >= 10 and i <= 21:
            continue;
  
        k = (i - 1) % 3;
        p2 = elp_terms[i];
        for j in range(len(p2) - 1, -1, -1):
            x = p2[j][0] * zeta + p2[j][1] * d + p2[j][2] * lp + \
                p2[j][3] * l + p2[j][4] * f + p2[j][5];
            y = p2[j][6];
            if (i >= 7 and i <= 9) or (i >= 25 and i <= 27):
                y *= t;
            if i >= 34 and i <= 36:
                y = (y * t) * t
            lbr2[k] += y * np.sin(x)

  
    """ Planetary perturbations, files elp10 to elp21"""
    for i in range(10,22):
        k = (i - 1) % 3;
        p3 = elp_terms[i];
        for j in range(len(p3)-1,-1,-1):
            x = p3[j][11] + p3[j][0] * me + p3[j][1] * ve + p3[j][2] * T + \
                p3[j][3] * ma + p3[j][4] * ju + p3[j][5] * sa + \
                p3[j][6] * ur + p3[j][9] * l + p3[j][10] * f
            if i <= 15:
                x += p3[j][7] * ne + p3[j][8] * d;
            else:
                x += p3[j][7] * d + p3[j][8] * lp;
            
            y = p3[j][12];
            if (i >= 13 and i <= 15) or (i >= 19 and i <= 21):
                y *= t;
            lbr3[k] += y * np.sin(x);
            
 
    """ Sum up the individual contributions"""
    V = util.modpi2(w + (lbr1[0] + lbr2[0] + lbr3[0]) * ACS_TO_RAD)
    U = (lbr1[1] + lbr2[1] + lbr3[1]) * ACS_TO_RAD;
    r = (lbr1[2] + lbr2[2] + lbr3[2])* 384747.9806448954 / 384747.9806743165;
 
    """ Convert to rectangular coordinates"""
    pos = [r * np.cos(V) * np.cos(U),
           r * np.sin(V) * np.cos(U),
           r * np.sin(U)]
    
 
    """ Set up precession matrix for conversion to the mean ecliptic of J2000"""
    P = (0.10180391E-4 +
        (0.47020439E-6 +
        (-0.5417367E-9 +
        (-0.2507948E-11 +
        0.463486E-14 * t) * t) * t) * t) * t;
 
    Q = (-0.113469002E-3 +
        (0.12372674E-6 +
        (0.12654170E-8 +
        (-0.1371808E-11 +
        -0.320334E-14 * t) * t) * t) * t) * t;
 
    pm=[[0.0 for i in range(0,3)],
       [0.0 for i in range(0,3)],
       [0.0 for i in range(0,3)]]
    pm[0][0] = 1.0 - (2.0 * P) * P;
    pm[0][1] = (2.0 * P) * Q;
    pm[0][2] = (2.0 * P) * np.sqrt(1.0 - P * P - Q * Q);
 
    pm[1][0] = pm[0][1];
    pm[1][1] = 1.0 - (2.0 * Q) * Q;
    pm[1][2] = (-2.0 * Q) * np.sqrt(1.0 - P * P - Q * Q);
 
    pm[2][0] = -pm[0][2];
    pm[2][1] = -pm[1][2];
    pm[2][2] = 1.0 - (2.0 * P) * P - (2.0 * Q) * Q;
 
    """ Rotate the moon's coordinates to the mean ecliptic of J2000"""
    pos = _rotate_rectangular(pm, pos);

    return _rectangular_to_spherical(pos, [0.0 for i in range(0,3)])


"""
 * Applies a rotation matrix to a body's rectangular coordinates.
 *
 * mat -- The rotation matrix.
 * pos -- The coordinates to be rotated in-place.
"""
def _rotate_rectangular(mat, pos):
 
    new_pos = [ (mat[0][0] * pos[0]) + (mat[0][1] * pos[1]) + (mat[0][2] * pos[2]),
           (mat[1][0] * pos[0]) + (mat[1][1] * pos[1]) + (mat[1][2] * pos[2]),
           (mat[2][0] * pos[0]) + (mat[2][1] * pos[1]) + (mat[2][2] * pos[2])
    ]
    return new_pos

def _rectangular_to_spherical(pla,ear):

    """ Convert from heliocentric coordinates to geocentric """
    x= pla[0] - ear[1];
    y = pla[1] - ear[1];
    z = pla[2] - ear[2];

    return [util.modpi2(np.arctan2(y, x)),
            np.arctan2(z, np.sqrt(x * x + y * y)),
            np.sqrt(x * x + y * y + z * z)]





    
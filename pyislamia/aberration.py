

"""
 * Implements the algorithm developed by C. Ron & J. Vondrak to calculate
 * the annual aberration using expansions of trigonometric series.
 *
 * Reference : <http://adsabs.harvard.edu/full/1986BAICz..37...96R>
"""

import numpy as np
from astroOps import fundamental_argument
from tool import to_julian_centuries
from pyislamia.tool import to_julian_centuries, sincos


"""
 * Terms for velocity components of the EMB's heliocentric motion #1
"""
aberration_series1_1 = [
    [1,-1719919,-2,-25,0,0,25,-13,-1,1578094,156,10,32,1,684187,-358],
    [2,6434,141,28007,-107,-1,25697,-95,-1,-5904,-130,11141,-48,0,-2559,-55],
    [3,486,-5,-236,-4,0,-216,-4,0,-446,5,-94,-2,0,-193,2],
]

"""
 * Terms for velocity components of the EMB's heliocentric motion #2
"""
aberration_series1_2 = [
    [0,0,2,0,-1,0,31,1,1,-28,0,-12],
    [0,0,3,-8,3,0,8,-28,25,8,11,3],
    [0,0,5,-8,3,0,8,-28,-25,-8,-11,-3],
    [0,1,0,0,0,0,-25,0,0,23,0,10],
    [0,2,-1,0,0,0,21,0,0,-19,0,-8],
    [0,0,1,0,-2,0,16,0,0,15,1,7],
    [0,0,1,0,1,0,11,-1,-1,-10,-1,-5],
    [0,2,-2,0,0,0,0,-11,-10,0,-4,0],
    [0,0,1,0,-1,0,-11,-2,-2,9,-1,4],
    [0,0,4,0,0,0,-7,-8,-8,6,-3,3],
    [0,0,3,0,-2,0,-10,0,0,9,0,4],
    [0,1,-2,0,0,0,-9,0,0,-9,0,-4],
    [0,2,-3,0,0,0,-9,0,0,-8,0,-4],
    [0,2,-3,0,0,0,0,-9,8,0,3,0],
    [0,0,3,-2,0,0,8,0,0,-8,0,-3],
    [0,8,-12,0,0,0,-4,-7,-6,4,-3,2],
    [0,8,-14,0,0,0,-4,-7,6,-4,3,-2],
    [0,0,0,2,0,0,-6,-5,-4,5,-2,2],
    [0,3,-4,0,0,0,-1,-1,-2,-7,1,-4],
    [0,0,2,0,-2,0,4,-6,-5,-4,-2,-2],
    [0,3,-3,0,0,0,0,-7,-6,0,-3,0],
    [0,0,2,-2,0,0,5,-5,-4,-5,-2,-2],
    [0,3,-6,0,0,0,4,-1,1,4,0,2],
    [0,0,0,0,1,0,-4,0,0,3,0,1],
    [0,0,9,-16,4,5,-1,-3,-3,1,-1,0],
    [0,0,7,-16,4,5,-1,-3,3,-1,1,0],
    [0,0,1,0,-3,0,3,1,0,3,0,1],
    [0,0,2,0,-3,0,3,-1,-1,1,0,1],
    [0,4,-5,0,0,0,-2,0,0,-3,0,-1],
    [0,0,1,-4,0,0,1,-2,2,1,1,1],
    [0,0,3,0,-3,0,-2,-1,0,2,0,1],
    [0,0,3,-4,0,0,1,-2,-2,-1,-1,0],
    [0,3,-2,0,0,0,2,0,0,-2,0,-1],
    [0,0,4,-4,0,0,2,-1,-1,-2,0,-1],
    [0,0,2,0,0,-1,2,0,0,-2,0,-1],
    [0,0,3,-3,0,0,2,-1,-1,-1,0,-1],
    [0,0,3,0,-1,0,0,-2,-1,0,-1,0],
    [0,0,1,0,0,1,0,-1,-1,0,-1,0],
    [0,0,0,0,2,0,-1,-1,-1,1,-1,0],
    [0,0,2,-1,0,0,1,0,0,-1,0,-1],
    [0,0,1,0,0,-1,0,-1,-1,0,-1,0],
    [0,5,-6,0,0,0,-2,0,0,-1,0,0],
    [0,0,1,-3,0,0,1,-1,1,1,0,0],
    [0,3,-6,4,0,0,-1,1,1,1,0,0],
    [0,3,-8,4,0,0,-1,1,-1,-1,0,0],
    [0,0,4,-5,0,0,1,-1,-1,0,0,0],
    [0,1,1,0,0,0,0,1,1,0,0,0],
    [0,3,-5,0,0,0,0,-1,1,0,0,0],
    [0,6,-7,0,0,0,-1,0,0,-1,0,0],
    [0,10,-9,0,0,0,1,0,0,-1,0,0],
    [0,0,2,-8,3,0,1,0,0,1,0,0],
    [0,0,6,-8,3,0,-1,0,0,1,0,0],
    [0,0,1,-2,0,0,1,0,0,1,0,0],
    [0,0,9,-15,0,0,-1,0,0,1,0,0],
    [0,0,1,0,-2,5,1,0,0,-1,0,0],
    [0,0,1,0,2,-5,-1,0,0,1,0,0],
    [0,0,1,0,0,-2,1,0,0,1,0,0],
    [0,0,0,1,0,0,-1,0,0,1,0,0],
    [0,0,7,-15,0,0,-1,0,0,-1,0,0],
    [0,2,0,0,0,0,0,-1,-1,0,0,0],
    [0,0,2,0,2,-5,0,1,1,0,0,0],
    [2,0,-2,0,0,0,0,1,-1,0,0,0],
    [0,0,9,-19,0,3,0,1,-1,0,0,0],
    [0,0,11,-19,0,3,0,1,1,0,0,0],
    [0,0,2,-5,0,0,0,-1,1,0,0,0],
    [0,5,-9,0,0,0,0,1,-1,0,0,0],
    [0,11,-10,0,0,0,1,0,0,0,0,0],
    [0,4,-4,0,0,0,0,1,0,0,0,0],
    [0,0,2,0,-4,0,1,0,0,0,0,0],
    [0,0,5,-6,0,0,0,-1,0,0,0,0],
    [0,5,-5,0,0,0,0,1,0,0,0,0],
    [0,0,4,0,-3,0,-1,0,0,0,0,0],
    [0,4,-6,0,0,0,0,-1,0,0,0,0],
    [0,5,-7,0,0,0,0,0,1,0,0,0],
    [0,0,4,0,-2,0,0,0,1,0,0,0],
    [0,0,3,0,-4,0,0,0,0,1,0,0],
    [0,7,-8,0,0,0,0,0,0,-1,0,0]
]

"""
 * Terms for velocity components of the Sun wrt the solar system barycenter
"""
aberration_series2 = [
    [0,0,1,0,0,0,719,0,6,-660,-15,-283],
    [0,0,0,1,0,0,159,0,2,-147,-6,-61],
    [0,0,2,0,0,0,34,-9,-8,-31,-4,-13],
    [0,0,0,0,1,0,17,0,0,-16,0,-7],
    [0,0,0,0,0,1,16,0,1,-15,-3,-6],
    [0,0,0,2,0,0,0,-9,-8,0,-3,1],
    [1,0,0,0,0,0,6,0,0,-6,0,-2],
    [0,1,0,0,0,0,5,0,0,-5,0,-2],
    [0,0,3,0,0,0,2,-1,-1,-2,0,-1],
    [0,0,1,-5,0,0,-2,0,0,-2,0,-1],
    [0,0,3,-5,0,0,-2,0,0,2,0,1],
    [1,0,0,0,0,-2,-1,0,0,-1,0,0],
    [0,0,0,3,0,0,-1,0,0,1,0,0],
    [0,0,2,-6,0,0,1,0,0,1,0,0],
    [0,0,2,-4,0,0,1,0,0,-1,0,0],
    [0,0,0,0,2,0,-1,0,0,1,0,0],
    [0,0,1,0,0,-2,1,0,0,0,0,0]
]

"""
 * Terms for velocity components of the Earth wrt the EMB
"""
aberration_series3 = [
    [1,0,0,0,0,715,-656,-285],
    [0,0,0,0,1,0,26,-59],
    [1,0,0,1,0,39,-36,-16],
    [1,2,0,-1,0,8,-7,-3],
    [1,-2,0,0,0,5,-5,-2],
    [1,2,0,0,0,4,-4,-2],
    [0,0,0,1,1,0,1,-3],
    [1,-2,0,1,0,-2,2,1],
    [1,0,0,2,0,2,-2,-1],
    [0,2,0,0,-1,0,1,-2],
    [1,0,0,0,-2,-1,1,1],
    [1,0,1,0,0,-1,1,0],
    [1,0,-1,0,0,1,-1,0],
    [1,4,0,-2,0,1,-1,0],
    [1,-2,0,2,0,-1,1,0],
    [1,2,0,1,0,1,0,0],
    [0,2,0,-1,1,0,0,-1]
]


def aberration_earth_velocity(jd):
    """
    * Calculates the components of the earth's velocity.
    *
    * jd -- TDB to be used for calculations. TT may be used for all but the most
    *        exacting applications.
    * Return:
    * vel -- The Earth's velocity components in 10**(-8) AU/day. The reference frame
    *        is the equinox & equator of J2000.
    """

    """ Fundamental arguments """
    t  = to_julian_centuries(jd)
    me = fundamental_argument("ARG_LONGITUDE_MERCURY", t)
    ve = fundamental_argument("ARG_LONGITUDE_VENUS", t)
    ea = fundamental_argument("ARG_LONGITUDE_EARTH", t)
    ma = fundamental_argument("ARG_LONGITUDE_MARS", t)
    ju = fundamental_argument("ARG_LONGITUDE_JUPITER", t)
    sa = fundamental_argument("ARG_LONGITUDE_SATURN", t)
    ur = fundamental_argument("ARG_LONGITUDE_URANUS", t)
    ne = fundamental_argument("ARG_LONGITUDE_NEPTUNE", t)
    l  = fundamental_argument("ARG_ANOMALY_MOON", t)
    lp = fundamental_argument("ARG_ANOMALY_SUN", t)
    f  = fundamental_argument("ARG_LATITUDE_MOON", t)
    d  = fundamental_argument("ARG_ELONGATION_MOON", t)
    w  = fundamental_argument("ARG_LONGITUDE_MOON", t)

    v1 = [ 0. for i in range(3)]
    v2 = [ 0. for i in range(3)]
    v3 = [ 0. for i in range(3)]

    """
     * Sum up each series individually with the smallest terms first
     * in order to maximize precision
    """

    """ EMB's heliocentric motion #2 """
    for i in range(len(aberration_series1_2)-1, -1, -1):
        phi = aberration_series1_2[i][0] * me + \
            aberration_series1_2[i][1] * ve + \
            aberration_series1_2[i][2] * ea + \
            aberration_series1_2[i][3] * ma + \
            aberration_series1_2[i][4] * ju + \
            aberration_series1_2[i][5] * sa

        [s_phi, c_phi] = sincos(phi)

        v1[0] += aberration_series1_2[i][6] * s_phi + \
            aberration_series1_2[i][7] * c_phi
        v1[1] += aberration_series1_2[i][8] * s_phi + \
            aberration_series1_2[i][9] * c_phi
        v1[2] += aberration_series1_2[i][10] * s_phi + \
            aberration_series1_2[i][11] * c_phi

    """EMB's heliocentric motion #1 """
    for i in range(len(aberration_series1_1)-1,-1,-1):
        phi = aberration_series1_1[i][0] * ea

        [s_phi,c_phi] = sincos(phi)

        v1[0] += (aberration_series1_1[i][1] + \
            aberration_series1_1[i][2] * t) * s_phi
        v1[0] += (aberration_series1_1[i][3] + \
            (aberration_series1_1[i][4] + \
            aberration_series1_1[i][5] * t) * t) * c_phi

        v1[1] += (aberration_series1_1[i][6] + \
            (aberration_series1_1[i][7] + \
            aberration_series1_1[i][8] * t) * t) * s_phi
        v1[1] += (aberration_series1_1[i][9] + \
            aberration_series1_1[i][10] * t) * c_phi

        v1[2] += (aberration_series1_1[i][11] +
            (aberration_series1_1[i][12] +
            aberration_series1_1[i][13] * t) * t) * s_phi
        v1[2] += (aberration_series1_1[i][14] +
            aberration_series1_1[i][15] * t) * c_phi
    
    """ The Sun's motion wrt the solar system barycenter """
    for i in range(len(aberration_series2)-1,-1,-1):
        phi = aberration_series2[i][0] * ve + \
            aberration_series2[i][1] * ea + \
            aberration_series2[i][2] * ju + \
            aberration_series2[i][3] * sa + \
            aberration_series2[i][4] * ur + \
            aberration_series2[i][5] * ne

        [s_phi,c_phi] = sincos(phi)

        v2[0] += aberration_series2[i][6] * s_phi + \
            aberration_series2[i][7]* c_phi
        v2[1] += aberration_series2[i][8] * s_phi + \
            aberration_series2[i][9] * c_phi
        v2[2] += aberration_series2[i][10] * s_phi + \
            aberration_series2[i][11] * c_phi
    
    """ The Earth's motion wrt to the EMB """
    for i in range(len(aberration_series3)-1,-1,-1):
        phi = aberration_series3[i][0]* w + \
            aberration_series3[i][1] * d + \
            aberration_series3[i][2] * lp + \
            aberration_series3[i][3] * l + \
            aberration_series3[i][4] * f

        [s_phi,c_phi] = sincos(phi)

        v3[0] += aberration_series3[i][5] * s_phi
        v3[1] += aberration_series3[i][6] * c_phi
        v3[2] += aberration_series3[i][7] * c_phi
    

    vel= [ v1[0] + v2[0] + v3[0],
           v1[1] + v2[1] + v3[1],
           v1[2] + v2[2] + v3[2]
        ]
    return vel


def annual_aberration(jd, equ_pos):
    """
    Calculates the annual aberration in right ascension and declination.
    Parameters:
       * jd -- julian day to be used for calculations.
       * equ_pos -- Equatorial coordinates of the celestial body.
    Return:
       * d_ra -- Aberration in right ascension.
       * d_dec -- Aberration in declination.
    """
#     double c,cra,sra,cdec,sdec;
#     struct rectangular_coordinates v;

    """ Speed of light in 10**(-8) AU/day """
    c = 17314463348.4

    """ Get the components of the Earth's velocity """
    v  = aberration_earth_velocity(jd)

    [sra,cra] = sincos(equ_pos[0])
    [sdec,cdec] = sincos(equ_pos[1])

    return [(v[1] * cra - v[0] * sra) / (c * cdec),
            (v[2] * cdec - (v[0] * cra + v[1] * sra) * sdec) / c
           ]


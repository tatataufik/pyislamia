"""@package pyislamia.hijri
Collection of functions for calculating hijriyah calendar.
"""

##
# @cvar HIJRI EPOCH = Friday, July 16, 622  CE (julian day = 1948439.5)
#
HIJRI_EPOCH = 1948439.5

###
# @cvar HIJRI_MONTHS
#
HIJRI_MONTHS = ['Muharram', 'Shafar', 'Rabiul Awal', 'Rabiul Akhir', 'Jumadil Awal', 'Jumadil Akhir', \
                'Rajab', 'Sya\'ban', 'Ramdhan', 'Syawal', 'Dzulqa\'dah', 'Dzulhijjah']

import numpy as np
import moon
from tool import start_of_day

##
# @cvar Start of Month criterion
#
MONTH_BEGIN_CRITERION = {"MABIMS": moon.mabimsStartOfMonth, \
                         "WUJUDUL_HILAL":  moon.wujudHilalStartOfMonth 
                         }



def get_hijriyah_date(jd,  obs_lon, obs_lat, tz_offset,start_at_maghrib=False, criterion = "MABIMS", obs_elevation = 0.0 ):
    """
    Get hijriyah date based on hilal information
    @param jd julian day at localtime
    @param obs_lon observer's geographical longitude
    @param obs_lat observer's geographical latitude
    @param criterion hijriyah beginning of month criterion (currently supported "MABIMS" and "WUJUDUL_HILAL")
    @param start_at_maghrib the flag to indicate whether start of day is at sunset (maghrib time), default = False
    @param obs_elevation observer's location height with respect to sea level (default=0.0)
    @return hijriyah date [day, month, year]
    """
    [d_est, m_est, y_est] = estimated_hijri_ymd(jd)\
    
    fn_start_of_month = MONTH_BEGIN_CRITERION[criterion] if MONTH_BEGIN_CRITERION[criterion] is not None else moon.mabimsStartOfMonth
    [jd_new_moon, jd_start_month, jd_full_moon,jd_next_new_moon] = moon.calcMoonParams(jd, obs_lon, obs_lat, tz_offset, obs_elevation, fn_start_of_month)
    
    if not start_at_maghrib:
        jd_start_month = start_of_day(jd_start_month + 1)
        
    day = int(np.floor(jd - jd_start_month + 1))
    
    m = m_est
    y = y_est
    
    '''adjust discrepancy from estimate at the end of month'''
    if (d_est == 30 or d_est == 29) and day == 1:
        m = m + 1
        if m > 12:
            m = 1
            y = y + 1
    elif d_est == 1 and (day == 30 or d_est == 29): 
        m = m - 1;
        if m < 1:
            m = 12
            y = y - 1
    

    
    return [day, m, y]

def estimated_hijri_ymd(jd_in):
    """
    Calculate estimated hijriyah date from given julian day based on hisab urfi.
    The number of days of each month are as follow:
      - Muharram     : 30
      - Shafar       : 29
      - Rabiul Awal  : 30
      - Rabiul Akhir : 29
      - Jumadil Awal : 30
      - Jumadil Akhir: 29
      - Rajab        : 30
      - Sya'ban      : 29
      - Ramadhan     : 30
      - Syawal       : 29
      - Dzul Qa'dah  : 30
      - Dzul Hijjah  : 29/30
    If the year is a leap year the number of days in month of Dzul Hijjah are 30 days. The sequence
    of leap year in a 30 year cycle is as follow: 2, 5, 7, 10, 13, 16, 18, 21, 24, 26, and 29. 
     
    @param jd_in julian day input.
    @return hijriyah [day, month, year] based on hisab urfi.
    """
    month = 0
    day = 0
    year = 0
    jd_in = start_of_day(jd_in)
                            
    if jd_in <= HIJRI_EPOCH:
        month = 0;
        day = 0;
        year = 0;
    else:
#         d_year = (jd_in - HIJRI_EPOCH)*30/10631 + 1 # for every 30 years has 10631 days
#         year= int(d_year)
#         day_of_year = (d_year - year)*10631/30
#         total_day = 0
#         month = 1
#         while total_day + _days_of_hijri_month(month,year) < day_of_year:
#             total_day+=_days_of_hijri_month(month,year)
#             month+=1
#         
#         if month > 12:
#             month = 1
#             year += 1
#             
#         
#         day = int(np.ceil(day_of_year - total_day))
        
        """Search forward year by year from approximate year"""
        year = np.floor((jd_in - HIJRI_EPOCH) / 355.0)
 
        while jd_in >= _jd_from_hijri_ymd(1, 1, year + 1):
            year= year + 1
             
        """Search forward month by month from Muharram"""
        month = 1;
        jd_end_month = _jd_from_hijri_ymd(month,
            _days_of_hijri_month(month, year), year)
        
        while jd_in >= jd_end_month + 1:
            month = month + 1;
            jd_end_month = _jd_from_hijri_ymd(month,
            _days_of_hijri_month(month, year), year)
               
        day = int(np.floor(jd_in - _jd_from_hijri_ymd(month, 1, year) + 1))
     
       
    return [day, month, year]

def _jd_from_hijri_ymd(month, day,year):
    return (day +  # days so far this month 
            29 * (month - 1) +  # days so far...
            np.floor(month / 2.0) +#            ...this year
            354 * (year - 1) + # non-leap days in prior years
            np.floor((3 + 11 * year) / 30.0) +  #// leap days in prior years
            HIJRI_EPOCH -1)#//
    
def _days_of_hijri_month(month, hijri_year):
    if (month % 2) == 1 or ((month == 12) and _is_hijriyah_leap_year(hijri_year)):
        return 30
    else:
        return 29

def _is_hijriyah_leap_year(hijri_year):
    """
    Cyle of 30 year: 11 years are leap years. The sequence is as follow : 2, 5, 7, 10, 13, 16, 18, 21, 24, 26, 29.
    """
    if ((((11 * hijri_year) + 14) % 30) < 11):
        return True
    else:
        return False
    
    
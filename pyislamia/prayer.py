"""@package pyislamia.prayer
Calculates the geocentric parallax in the equatorial coordinates of a celestial body
"""

from pyislamia.tool import start_of_day, frac_of_day
from astronomia import util as u, nutation, constants, sun, coordinates
import solar
import numpy as np


##
# @cvar standard angle for various authority (shubuh_angle,sun rise/set angle, isha_angle).
#
STANDARD_ANGLE = { "MABIMS": [u.d_to_r(-20.0),u.d_to_r(-.83333),u.d_to_r(-18.0)], \
                  "MUSLIM_LEAGUE_WORLD": [u.d_to_r(-18.0),u.d_to_r(-.83333), u.d_to_r(-18.0)] \
                 }


##
# @cvar Asr jurisdidiction based on HANAFI or SHAFII school of tought.
#
ASR_JURISDICTION = { "SHAFII": 1.0, \
                     "HANAFI":2.0 \
                   }


def get_prayer_times(jd,standard_angle, asr_jurisdiction, obs_lon, obs_lat, tz_offset, elevation=0.0 ):
    """
    Calculate muslim prayer times.
    @param jd julian day (local time)
    @param standard_angle the desired shalat angle standard
    @param asr_jurisdiction the desired asr jurisdiction
    @param obs_lon observer's geographic longitude
    @param obs_lat observer's geographic latitude
    @param tz_offset timezone offset in timedelta
    @param elevation observer's height with respect to sea level
    @return [julian_day at 0H, shubuh time, syuruq time, dhuhr time, asr time, maghrib time, isha time] all the times are expressed in fraction of day.
    """
    start_jd = start_of_day(jd)
    
    eqT =  solar.equationOfTime(jd - frac_of_day(tz_offset), obs_lon, obs_lat, tz_offset) #equation of time
    
    
    transit = 0.5 + frac_of_day(tz_offset) - obs_lon/constants.pi2 - eqT #transit time (dhuhr time)
    
    obliquity = nutation.obliquity(jd-frac_of_day(tz_offset))
    _sun = sun.Sun()
    ecl_pos = _sun.dimension3(jd-frac_of_day(tz_offset)) #sun ecliptical position
    [sun_ra, sun_decl] = coordinates.ecl_to_equ(ecl_pos[0], ecl_pos[1], obliquity) #sun equatorial position
    
    '''syuruq time'''
    rise_time  = transit  - np.arccos((np.sin(standard_angle[1]-u.d_to_r(0.0347*np.sqrt(elevation))) - \
                              np.sin(obs_lat)*np.sin(sun_decl))/ \
                             (np.cos(obs_lat)*np.cos(sun_decl))
                             )/constants.pi2
    '''maghrib'''
    set_time  = transit  + np.arccos((np.sin(standard_angle[1]-u.d_to_r(0.0347*np.sqrt(elevation))) - \
                              np.sin(obs_lat)*np.sin(sun_decl))/ \
                             (np.cos(obs_lat)*np.cos(sun_decl))
                             )/constants.pi2
    '''shubuh time'''
    shubuh_time = transit - np.arccos((np.sin(standard_angle[0]) - \
                              np.sin(obs_lat)*np.sin(sun_decl))/ \
                             (np.cos(obs_lat)*np.cos(sun_decl))
                             )/constants.pi2
    
    '''asr time'''
    asr_angle = np.arctan(1.0/(asr_jurisdiction + np.tan(np.fabs(obs_lat-sun_decl))))     
    asr_time =transit + np.arccos((np.sin(asr_angle) - \
                              np.sin(obs_lat)*np.sin(sun_decl))/ \
                             (np.cos(obs_lat)*np.cos(sun_decl))
                             )/constants.pi2
                       
    '''isya time'''
    isha_time = transit + np.arccos((np.sin(standard_angle[2]) - \
                              np.sin(obs_lat)*np.sin(sun_decl))/ \
                             (np.cos(obs_lat)*np.cos(sun_decl))
                             )/constants.pi2
   
    return [start_jd, shubuh_time, rise_time, transit, asr_time, set_time, isha_time]

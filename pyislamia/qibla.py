"""@package pyislamia.qibla
Collection of functions for calculating moon parameters
"""

import numpy as np
from astronomia import util,nutation, sun,coordinates,calendar
import constants as c
from pyislamia import solar
from pyislamia.tool import frac_of_day, start_of_day

def qiblaDirection(obs_lon, obs_lat):
        """
        Calculate qibla direction
        @param obs_lon geographical longitude of observer in radians
        @param obs_lat geographical latitude of observer in radians
        @return qibla direction in radians
        """
        
        x = np.cos(obs_lat) * np.tan(c.KAABAH_LAT) - np.sin(obs_lat) * np.cos(c.KAABAH_LON - obs_lon)
        y = np.sin(c.KAABAH_LON - obs_lon)

        direction = np.arctan2(y, x);
       
        direction =  util.pi2 + direction if direction < 0 else direction

        return direction
    
    
def qiblaHour(jd, obs_lon,obs_lat, tz_offset):
        """
        Calculate time at which shadow from sun is toward or against qibla direction
        @param obs_lon geographical longitude of observer in radians
        @param obs_lat geographical latitude of observer in radians
        @tz_offset
        @return julian day of qibla hour
        """
        
        q = util.pi2 - qiblaDirection(obs_lon, obs_lat)
        
       
        p = np.arctan(1./(np.tan(q)*np.sin(obs_lat)))
       
        '''obliquity value'''
        obliquity = nutation.obliquity(jd - frac_of_day(tz_offset))
        
        '''load sun data (vsop87)'''
        _sun = sun.Sun()
        '''sun ecliptical position'''
        ecl_pos = _sun.dimension3(jd - frac_of_day(tz_offset))
        '''sun equatorial position'''
        equ_pos = coordinates.ecl_to_equ(ecl_pos[0],ecl_pos[1],obliquity)
        
        c = np.arccos((np.cos(p)*np.tan(equ_pos[1]))/np.tan(obs_lat)) + p
      
        '''hour angle of qibla time'''
        dqibla_ha = (c - obs_lon)/util.pi2 + frac_of_day(tz_offset)
        
        '''equation of time'''
        eqT = solar.equationOfTime(jd, obs_lon, obs_lat, tz_offset)
        
        '''julian day at which shadow from sun is either facing or againts qibla direction'''
        t_qibla = dqibla_ha + 0.5 - eqT
        
        '''get rise and set time for validation'''
        rise_set = solar.sunRiseSet(jd, obs_lon, obs_lat, tz_offset)
        
        if t_qibla > rise_set[0] and t_qibla < rise_set[1]:
            '''check if the qibla_hour occur during day time'''
            jd_qibla = start_of_day(jd) + dqibla_ha + 0.5 - eqT
        
            return jd_qibla
        else:
            return None
        
    
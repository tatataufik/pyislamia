"""@package pyislamia.moon
Collection of functions for calculating moon parameters
"""

from astronomia import planets,lunar, util, nutation, calendar,coordinates, constants,sun
import numpy as np
from pyislamia.constants import SYNODIC_MONTH, MOON_ALT
import astroOps, aberration
from datetime import timedelta
import search
import solar
import riseset
import constants as c
from pyislamia.tool import start_of_day, jd_to_datetime, frac_of_day
from pyislamia import astroOps, parallax



def ageOfMoonInDays(jd):
    """
    The function is used to calculate age of moon in day value since last new moon.
    
    @param jd julian_day (UTC).
    @return number of days from last new moon
    """
    
    age_in_day = SYNODIC_MONTH*ageOfMoonInRad(jd)/util.pi2
    return age_in_day


def ageOfMoonInRad(jd):
    """
    The function is used to calclulate age of moon in radian since last new moon.
    
    @param jd julian_day (UTC).
    @return age of moon in radians (0 - 2*pi) from last new moon
    """
    
    _sun = sun.Sun() #Load VSOP87 data
    
    
    '''Ecliptical longitude of sun'''
    sunLon = _sun.dimension(jd, "L")
    
    '''Ecliptical longitude of moon'''
    moonLon = _moon_ecl_pos(jd)[0]
   
    '''age of moon in days'''
    age = util.modpi2(util.pi2 - (sunLon - moonLon))
    return age

def moonAzAlt(jd, obs_lon, obs_lat):
    """
    The function is used to calculate at azimuth and altitude of moon at a particular julian_day (UTC), given latitude and longitude of the observer.
    
    @param jd julian day (UTC)
    @param obs_lon observer longitude in radians
    @param obs_lat observer latitude in radians
    @return azimuth and altitude in radians  
    """
    
    obliquity = nutation.obliquity(jd)
    obliquity += nutation.nutation_in_longitude(jd)
     
    '''local siderial time'''
    localSiderealTime =astroOps.apparent_greenwich_siderial_time(jd)+ obs_lon 

    
    ecl_pos = _moon_ecl_pos(jd)
#     print map( lambda x: util.r_to_d(x),ecl_pos[:2])
    '''lunar equatorial position'''
    equ_pos = coordinates.ecl_to_equ(ecl_pos[0],ecl_pos[1], obliquity)
    '''lunar horizontal position (Az,Alt)'''
    hor = astroOps.equ_to_horiz(localSiderealTime - equ_pos[0],equ_pos[1],obs_lat) 
    hor = [util.modpi2(hor[0]-np.pi),hor[1]]
    '''return lunar horizontal position (Az,Alt)'''
    return hor
    
    
def hilalInfo(jd_new_moon, obs_lon, obs_lat, tz_offset, obs_elevation=0.):
    """
    Calculate of hilal information of the hijri month at sun set ond the same day of jd_new_moon.
    @param jd_new_moon julian day of new moon (localtime).
    @param obs_lon observer longitude.
    @param obs_lat observer latitude.
    @return [jd_sun_set, jd_moon_set, moon_altitude, elongation, age of moon at sun set ]
    """
    
    jd_start_of_day = start_of_day(jd_new_moon) 
    
    '''find time at which the sun is setting'''
    tp = solar.sunRiseSet(jd_new_moon, obs_lon, obs_lat, tz_offset)
    jd_sun_set = jd_start_of_day + tp[1]
    
    '''ecliptical position of the moon during sun set'''
    ecl_pos = _moon_ecl_pos(jd_sun_set- frac_of_day(tz_offset))
    obliquity = nutation.obliquity(jd_sun_set - frac_of_day(tz_offset)) + nutation.nutation_in_obliquity(jd_sun_set - frac_of_day(tz_offset))
    nutInLongitude = nutation.nutation_in_longitude(jd_sun_set - frac_of_day(tz_offset))
    
    
    '''equatorial position of the moon during sun set'''
    equ_pos = coordinates.ecl_to_equ(ecl_pos[0] + nutInLongitude, ecl_pos[1], obliquity)
    
    '''calculate aberration'''
    delta_equ_pos = aberration.annual_aberration(jd_sun_set - frac_of_day(tz_offset), equ_pos)
    equ_pos = [equ_pos[i] + delta_equ_pos[i] for i in range(2)]
    
   
    '''hour angle during sun set''' 
    hour_angle = astroOps.calc_hour_angle(jd_sun_set -  frac_of_day(tz_offset),
                                           obs_lon, 
                                           equ_pos[0])
    '''parallax correction'''
    delta_equ_pos = parallax.geocentric_parallax( hour_angle, 
                                    equ_pos[1],  
                                    ecl_pos[2]/c.AU, 
                                    obs_lat, 
                                    obs_elevation)
    
    equ_pos = [equ_pos[i] + delta_equ_pos[i] for i in range(2)]
                 
    '''right ascension and declination of moon during sun set'''
    ra1 = equ_pos[0]
    dec1= equ_pos[1]
    
    
    '''hour angle of the moon'''
    H = astroOps.calc_hour_angle(jd_sun_set-frac_of_day(tz_offset), obs_lon, ra1)
    
    '''altitidue of the moon'''
    h1 = astroOps.equ_to_horiz(H, dec1, obs_lat)[1]
#     print "moon alt: %s" % str(util.d_to_dms(util.r_to_d(h1)))
    
    '''load vsop87 data'''
    _sun = sun.Sun()
    ecl_pos2 = _sun.dimension3(jd_sun_set-frac_of_day(tz_offset))
    equ_pos2 = coordinates.ecl_to_equ(ecl_pos2[0], ecl_pos2[1], obliquity)
    ra2 = equ_pos2[0]
    dec2 = equ_pos2[1]
    
    '''calculate elongation moon - earth - sun'''
    elongation = astroOps.calc_elongation(ra1, dec1, ra2, dec2)
    
    moon_riseset = moonRiseSet(jd_sun_set, obs_lon, obs_lat, tz_offset)
    jd_moon_set = jd_start_of_day + moon_riseset[1]
    
    return [jd_sun_set,jd_moon_set, h1, elongation, jd_sun_set - jd_new_moon]

def wujudHilalStartOfMonth(jd_new_moon, obs_lon, obs_lat, tz_offset, obs_elevation=0.):
    """
    Calculate jd at which the current hijri month started based on wujudul hilal criterion 
    given the new moon julian day and observer's longitude, latitude and timezone.
    
    @param jd_new_moon julian day of last new moon (UTC)
    @param obs_lon observer's longitude
    @param obs_lat observer's latitude
    
    @return the julian day at which the current hijri month started
    """
#     _lunar = lunar.Lunar() #check visibility of hilal given location
    jd_start_of_day = start_of_day(jd_new_moon) 
    
#     '''find time at which the sun is setting'''
#     tp = solar.sunRiseSet(jd_new_moon, obs_lon, obs_lat, tz_offset)
#     jd_sun_set = jd_start_of_day + tp[1]

    '''find time at which the sun is setting for next day'''
    tp = solar.sunRiseSet(jd_new_moon+1, obs_lon, obs_lat, tz_offset)
    jd_next_sun_set = jd_start_of_day + 1 + tp[1]
    
    
    [jd_sun_set, jd_moon_set, h1, elongation, age_of_moon ] = hilalInfo(jd_new_moon, obs_lon, obs_lat, tz_offset, obs_elevation)
    
    '''start of month in julian_day based on Wujudul hilal criterion (sun set before the moon does'''
    if jd_sun_set < jd_moon_set:
        jd_start_of_month = jd_sun_set
    else:
        jd_start_of_month = jd_next_sun_set
        
    return jd_start_of_month
    
    
        
def mabimsStartOfMonth(jd_new_moon, obs_lon, obs_lat, tz_offset, obs_elevation=0.):
    """
    Calculate jd at which the current hijri month started based on MABIMS criterion 
    given the new moon julian day and observer's longitude, latitude and timezone.
    
    @param jd_new_moon julian day of last new moon (UTC)
    @param obs_lon observer's longitude
    @param obs_lat observer's latitude
    
    @return the julian day at which the current hijri month started
    """
#     _lunar = lunar.Lunar() #check visibility of hilal given location
    jd_start_of_day = start_of_day(jd_new_moon) 
    
#     '''find time at which the sun is setting'''
#     tp = solar.sunRiseSet(jd_new_moon, obs_lon, obs_lat, tz_offset)
#     jd_sun_set = jd_start_of_day + tp[1]

    '''find time at which the sun is setting for next day'''
    tp = solar.sunRiseSet(jd_new_moon+1, obs_lon, obs_lat, tz_offset)
    jd_next_sun_set = jd_start_of_day + 1 + tp[1]
    
    
    [jd_sun_set, jd_moon_set, h1, elongation, age_of_moon ] = hilalInfo(jd_new_moon, obs_lon, obs_lat, tz_offset, obs_elevation)
    
    '''start of month in julian_day based on MABIMS criterion (more than 2 degree altitude and more than 3 degree elongation or the age of moon more than 8 hours'''
    if (elongation >= util.d_to_r(3.0) and h1 >= util.d_to_r(2.0)) or age_of_moon * 24. >= 8.:
        jd_start_of_month = jd_sun_set
    else:
        jd_start_of_month = jd_next_sun_set
        
    return jd_start_of_month


def newMoon(jd_estimated, tz_offset):
    """
    Calculate new moon based on an estimated julian day (local time) using 3 point searchMinima
    @param jd_estimated estimated julian day (local time) 
    @param tz_offset local time offset from UTC in timedelta
    @return julian day at which new moon occurs
    """
    
    delta = 5. / 24. #five hours
    
    jd_start = start_of_day(jd_estimated) - 1.
    
    '''datetime format'''
    cal = jd_to_datetime(jd_estimated)
    fractions = [ 
                 0.0,
                 1.0 + cal.hour / 24.0 + cal.minute/1440. + cal.second/86400.,
                 2.0
                 ]
    ages = [
            _moon_eval(jd_start + fractions[0] - frac_of_day(tz_offset), 0.0),
            _moon_eval(jd_start + fractions[1]- frac_of_day(tz_offset), 0.0),
            _moon_eval(jd_start + fractions[2]- frac_of_day(tz_offset), 0.0)
            ]
    '''make sure age[0] >= age[1]'''
    while ages[0] < ages[1]:
            fractions[0] -= delta
            ages[0] = _moon_eval(jd_start + fractions[0]- frac_of_day(tz_offset), 0.0)
            
    '''make sure age[2] >= age[1]'''
    while ages[2] < ages[1]:
            fractions[2] += delta;
            ages[2] = _moon_eval(jd_start + fractions[2]- frac_of_day(tz_offset), 0.0);
            
    '''search jd at new moon by searching minima of age with target is 0 radian, given jd1<jd2<jd3 and age[jd1]>age[jd2], age[jd2]<age[jd3]'''
    jd_new_moon = jd_start + search.searchMinima(jd_start- frac_of_day(tz_offset), 
                                                 0., 
                                                 0.,
                                                 tz_offset,
                                                 0.0, 
                                                 fractions, 
                                                 ages, 
                                                 _moon_eval)

    return jd_new_moon

def fullMoon(jd_estimated, tz_offset):
    """
    Calculate full moon based on an estimated julian day (local time) using 3 point searchMinima
    @param jd_estimated estimated julian day (local time) 
    @param tz_offset local time offset from UTC in timedelta
    @return julian day at which full moon occurs
    """
     
    delta = 5. / 24. #five hours
  
    jd_start = start_of_day(jd_estimated) - 1.0
    cal = jd_to_datetime(jd_estimated)

    '''initial value of x1,x2,x3 and f(x1),f(x2), f(x3)'''
    fractions =[ 0,
                1.5 + cal.hour / 24.0  + cal.minute / 1440. + cal.second / 86400.,
                3.0
                ]
    ages = [ _moon_eval(jd_start + fractions[0]- frac_of_day(tz_offset), constants.pi),
             _moon_eval(jd_start + fractions[1]- frac_of_day(tz_offset), constants.pi),
             _moon_eval(jd_start + fractions[2]- frac_of_day(tz_offset), constants.pi)
           ]

    '''make sure age[0] >= age[1]'''
    while (ages[0] < ages[1]):
        fractions[0] -= delta
        ages[0] = _moon_eval(jd_start + fractions[0]- frac_of_day(tz_offset), constants.pi);
        
    '''make sure age[2] >= age[1]'''
    while (ages[2] < ages[1]):
        fractions[2] += delta;
        ages[2] = _moon_eval(jd_start + fractions[2]- frac_of_day(tz_offset), constants.pi);
    
    '''search minima with target is PI'''
    jd_full_moon = jd_start + search.searchMinima(jd_start- frac_of_day(tz_offset),
                                                  0., 
                                                  0.,
                                                  tz_offset,
                                                  constants.pi, 
                                                  fractions, 
                                                  ages,
                                                  _moon_eval)
    
    return jd_full_moon
    
def calcMoonParams(jd, obs_lon, obs_lat, tz_offset, obs_elevation = 0.0, fn_start_of_month = mabimsStartOfMonth):
    """
    Calculate [jd_new_moon, jd_start_month, jd_full_moon,jd_next_new_moon]
    @param jd julian day of local time
    @param obs_lon observers' longitude
    @param obs_lat observers' latitude
    @param tz_offset offset from UTC in timedelta
    @param fn_start_of_month function to determin start of moonth currently supported mabimsStartOfMonth() and wujudHilalStartOfMonth()
    @return [jd_new_moon, jd_start_month, jd_full_moon,jd_next_new_moon]
    """
    ageOfMoon = ageOfMoonInDays(jd-frac_of_day(tz_offset))
    jd_approx_new_moon = jd - ageOfMoon;

    jd_new_moon = None
    jd_start_month = None
   
    flag_break = False

    '''stop the loop if the flag_break is true'''
    while not flag_break:
        
        '''calc new moon'''
        jd_new_moon = newMoon(jd_approx_new_moon, tz_offset)

        '''determining start of hijriyah man in julian day''' 
        rise_set = solar.sunRiseSet(jd, obs_lon, obs_lat, tz_offset)
        jd_start_month = fn_start_of_month(jd_new_moon, obs_lon, obs_lat, tz_offset)
        jd_start_month = np.floor(jd_start_month) - 0.5 + rise_set[1];
       
        '''these steps are added if the end result jd_start_month > jd (current)'''      
        jd_approx_new_moon = jd_new_moon - SYNODIC_MONTH - 1
        
        '''stop criteria: jd_start_month > jd (current)'''
        flag_break = jd_start_month <= jd
        
    '''calc next new moon'''
    jd_approx_next_new_moon = jd_new_moon + SYNODIC_MONTH
  
    '''next new moon'''
    jd_next_new_moon = newMoon(jd_approx_next_new_moon, tz_offset)
    
    '''get length of month'''
    moon_period = jd_next_new_moon - jd_new_moon
    
    '''init to calculate jd full moon'''
    jd_approx_full_moon = jd_new_moon + moon_period / 2.0
 
    jd_full_moon = fullMoon(jd_approx_full_moon, tz_offset)
    
    '''return julian day values for last new moon, start of current hijri month, full moon, and next new moon''' 
    return [jd_new_moon, jd_start_month, jd_full_moon,jd_next_new_moon]
        
def moonTransit(jd,obs_lon, obs_lat, tz_offset):
    """
    The function to calculate the transit time of the moon (moon located in the culmination point with respect to observer) given a julian day
    @param jd julian day
    @param obs_lon observer longitude (negative value for west and positive for east)
    @param obs_lat observer latitude (negative value for south and positive for north)
    
    @return the setting and rising time of the moon for given julian day
    """
    jd_start = start_of_day(jd) #julian day start of day
    raList = [0.,0.,0.]
    decList = [0.,0.,0.]
    _lunar = lunar.Lunar()
    
    ''' calculate the equatorial position at jd_start-1 0H, jd 0H, jd+1 0H '''
    for i in range(0,3):
        t  = jd_start + i-1 - frac_of_day(tz_offset)
        x = _lunar.dimension3(t)
        obliquity = nutation.obliquity(t)
        equ_pos = coordinates.ecl_to_equ(x[0], x[1], obliquity)
        
        raList[i] = equ_pos[0]
    
    
        
    '''transit time'''
    t = riseset.transit(jd_start - frac_of_day(tz_offset), raList, 1.0/constants.minutes_per_day,obs_lon, obs_lat)
    
    transit_time = t + frac_of_day(tz_offset) - jd_start if t is not None else None
    
    return transit_time
  

def moonRiseSet(jd,obs_lon, obs_lat, tz_offset):
    """
    The function to calculate the rising and setting time of the moon given a julian day
    @param jd julian day
    @param obs_lon observer longitude (negative value for west and positive for east)
    @param obs_lat observer latitude (negative value for south and positive for north)
    
    @return the setting and rising time of the moon for given julian day
    """
    
    jd_start = start_of_day(jd) #julian day start of day
    raList = [0.,0.,0.]
    decList = [0.,0.,0.]
    _lunar = lunar.Lunar()
    
    ''' calculate the equatorial position at jd_start-1 0H, jd 0H, jd+1 0H '''
    for i in range(0,3):
        t  = jd_start+i-1 - frac_of_day(tz_offset)
        x = _lunar.dimension3(t)
        obliquity = nutation.obliquity(t)
        
        '''ecliptical position'''
        equ_pos = coordinates.ecl_to_equ(x[0], x[1], obliquity)
        
        raList[i] = equ_pos[0]
        decList[i] = equ_pos[1]
    
    '''set time'''
    t = riseset.settime(jd_start - frac_of_day(tz_offset), raList, decList, MOON_ALT, 1.0/constants.minutes_per_day,obs_lon, obs_lat)
    set_time = t  + frac_of_day(tz_offset) - jd_start if t is not None else None
    
    '''rise time'''
    t = riseset.rise(jd_start - frac_of_day(tz_offset), raList, decList, MOON_ALT, 1.0/constants.minutes_per_day, obs_lon, obs_lat)
    rise_time = t + frac_of_day(tz_offset) - jd_start if t is not None else None
    
    return [rise_time, set_time]
        
        
        
def _moon_ecl_pos(jd):

    
    _lunar = lunar.Lunar() #load ELP-2000 data
    '''Ecliptical longitude of moon'''
    return _lunar.dimension3(jd)
        


def _moon_eval(jd, target, obs_lon=0., obs_lat=0., tz_offset=timedelta(seconds=0)):    
        age_of_moon = ageOfMoonInRad(jd);
        if (age_of_moon > 1.7 * np.pi):
            age_of_moon -= util.pi2
            
        return np.abs(age_of_moon - target);
"""@package pyislamia.solar
Collection of functions for calculating solar parameters
"""

from astronomia import sun, util, nutation,coordinates,constants
import numpy as np
from constants import SUN_ALT, HOURS_PER_DAY
import astroOps
import riseset
from pyislamia.tool import start_of_day, frac_of_day
from pyislamia.astroOps import calc_hour_angle


def sunAzAlt(jd, obs_lon, obs_lat):
    """
    Finding sun azimuth and altitude given observer longitude and latitude.
    @param jd julian day in UTC
    @param obs_lon observer longitude in radians
    @param obs_lat observer latitude in radians
    @return azimuth and altitude of sun with respect to observer in radians
    """   
    obliquity = nutation.obliquity(jd) #calc obliquity

    '''load solar data'''
    _sun = sun.Sun()
    '''solar ecliptical position'''
    ecl_pos = _sun.dimension3(jd)

    '''solar equatorial position'''
    equ_pos = coordinates.ecl_to_equ(ecl_pos[0],ecl_pos[1], obliquity)
    '''solar horizontal position (Az,Alt)'''
    hor = astroOps.equ_to_horiz(calc_hour_angle(jd,obs_lon, equ_pos[0]),equ_pos[1],obs_lat) 
    hor = [util.modpi2(hor[0]-np.pi),hor[1]]
    return hor

def sunAzAltWholeDay(jd,obs_lon,obs_lat,tz_offset):
    """
    Finding sun azimuth and altitude for whole day with interval 1 hour, given observer longitude, latitude and timezone info.
    @param jd julian day
    @param obs_lon observer longitude in radian
    @param obs_lat observer latitude in radian
    @param tz_offset offset time from UTC as datetime.timedelta obj
    @return sun azimuth and altitude for whole day with interval 1 hour
    """
    floor_jd = np.floor(jd)
    jd_start = floor_jd + 0.5 if jd - floor_jd >= 0.5 else floor_jd - 0.5
   
    '''calc julian day for each hour'''
    t = map(lambda y: jd_start + y/24. - frac_of_day(tz_offset),range(0,HOURS_PER_DAY+1))
   
    return map(lambda x: sunAzAlt(x,obs_lon, obs_lat),t)
        

def sunRiseSet(jd,obs_lon, obs_lat, tz_offset,elevation=0.):
    """
    The function to calculate the rising and setting time of the sun given a julian day
    @param jd julian day of local time
    @param obs_lon observer longitude (negative value for west and positive for east)
    @param obs_lat observer latitude (negative value for south and positive for north)
    @param tz_offset offset local time from UTC in timedelta
    @param elevation observer elevation from sea level in meters
    
    @return the setting and rising time of the moon for given julian day
    """
    
    jd_start = start_of_day(jd)
    raList = [0.,0.,0.]
    decList = [0.,0.,0.]
    _sun = sun.Sun()
    ''' calculate the equatorial position at jd_start-1 0H, jd 0H, jd+1 0H '''
    for i in range(0,3):
        t  = jd_start+i-1 - frac_of_day(tz_offset)
        x = _sun.dimension3(t)
        obliquity = nutation.obliquity(t)
        equ_pos = coordinates.ecl_to_equ(x[0], x[1], obliquity)
        raList[i] = equ_pos[0]
        decList[i] = equ_pos[1]
    
    adjust_alt = -util.d_to_r(0.0347*np.sqrt(elevation))
    '''set time'''
    rise_time = riseset.rise(jd_start - frac_of_day(tz_offset), raList, decList, SUN_ALT+adjust_alt, 1.0/constants.minutes_per_day, obs_lon, obs_lat) 
    rise_time = rise_time + frac_of_day(tz_offset) - jd_start if rise_time is not None else None
    
    '''rise time'''
    set_time = riseset.settime(jd_start - frac_of_day(tz_offset), raList, decList, SUN_ALT+adjust_alt, 1.0/constants.minutes_per_day,obs_lon, obs_lat) 
    set_time = set_time + frac_of_day(tz_offset)  - jd_start if set_time is not None else None
    
    return [rise_time, set_time]



def sunTransit(jd,obs_lon, obs_lat, tz_offset):
    """
    The function to calculate the transit time of the sun (sun located at the culminated position with respect to observer)
    given a julian day.
    @param jd julian day in local time
    @param obs_lon observer longitude (negative value for west and positive for east)
    @param obs_lat observer latitude (negative value for south and positive for north)
    
    @return the transit time of the sun in fraction of day with respect to 0H local time
    """
    
    jd_start = start_of_day(jd)
    raList = [0.,0.,0.]
    decList = [0.,0.,0.]
    _sun = sun.Sun()
    ''' calculate the equatorial position at jd_start-1 0H, jd 0H, jd+1 0H '''
    for i in range(0,3):
        t  = jd_start+i-1 - frac_of_day(tz_offset)
        x = _sun.dimension3(t)
        obliquity = nutation.obliquity(t)
        equ_pos = coordinates.ecl_to_equ(x[0], x[1], obliquity)
        raList[i] = equ_pos[0]
       
    
    '''transit time'''
    transit = riseset.transit(jd_start - frac_of_day(tz_offset), raList, 1.0/constants.minutes_per_day, obs_lon, obs_lat) 
    transit = transit + frac_of_day(tz_offset) - jd_start if transit is not None else None
    
    return transit


def equationOfTime(jd,obs_lon, obs_lat, tz_offset):
    """
    Equation of time calculation.
    @param jd julian day of local time.
    @param obs_lon observer's geographical longitude.
    @param obs_lat observer's geographical latitude.
    @param tz_offset offset from UTC in datetime.timedelta.
    @return equation of time in fraction of day.
    """
   
    transit_time = sunTransit(jd, obs_lon, obs_lat, tz_offset)  #sun transit time
        
    '''equation of time'''
    eqT = 0.5 - transit_time  + (frac_of_day(tz_offset) - obs_lon/util.pi2)
    
    return eqT
      

# def sunRiseSet(jd, obs_lon, obs_lat, tz_offset):
#     floor_jd = np.floor(jd)
#     jd_start = floor_jd + 0.5 if jd - floor_jd >= 0.5 else floor_jd - 0.5
#     
#     risesetAlt = SUN_ALT
#     altitude = sunAzAltWholeDay(jd, obs_lon, obs_lat, tz_offset)
#     
#    
#     x_rise_set = [-1,-1]
#     for i in range(0, HOURS_PER_DAY):
#         index = -1
#         if (altitude[i][1] <= 0. and altitude[i + 1][1] > 0.):
#             ''' check for rising '''
#             index = 0
#         elif altitude[i][1] > 0. and altitude[i + 1][1] <= 0.:
#             ''' check for setting '''
#             index = 1
#         
#         if (index != -1):
#             fraction = i/24.
#             alt0 = altitude[i][1]
#             altDiff = altitude[i+1][1] - alt0
#             
#             fraction = refineSearch(jd_start, fraction, alt0, altDiff, obs_lon, obs_lat, tz_offset,  risesetAlt, rise_set_eval);
#             x_rise_set[index] = fraction
#             
#     return x_rise_set

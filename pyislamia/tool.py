"""@package pyislamia.tool
Collection of functions for calculating moon parameters
"""

from astronomia import calendar
from datetime import datetime,timedelta
import numpy as np
import constants as c

def jd_to_datetime(jd):
    """
    Convert julian day to datetime.
    """
    ymd = calendar.jd_to_cal(jd)
    start_day = start_of_day(jd)
    return datetime(year = int(ymd[0]), month= int(ymd[1]), day=int(ymd[2])) + frac_of_day_to_timedelta(calendar.fday_to_hms(jd - start_day))

def frac_of_day_to_timedelta(frac_day):
    """
    Convert fraction of day to timedelta.
    """
    return timedelta(hours=frac_day[0],minutes=frac_day[1],seconds=int(frac_day[2]))

def start_of_day(jd):
    """
    Calculate julian day at 0H
    """
    return np.floor(jd) + 0.5 if jd -np.floor(jd)>= 0.5 else np.floor(jd) - 0.5

def d_to_dms(x):
    """
    Convert an angle in decimal degrees to degree components.

    Return a tuple (degrees, minutes, seconds). Degrees and minutes
    will be integers, seconds may be floating.

    If the argument is negative:

        The return value of degrees will be negative.
        If degrees is 0, minutes will be negative.
        If minutes is 0, seconds will be negative.

    Arguments:
      - `x` : degrees

    Returns:
      - degrees : (int)
      - minutes : (int)
      - seconds : (int, float)

    """
    frac, degrees = np.modf(x)
    seconds, minutes = np.modf(frac*60)
    sign = (-1. if x<0. else 1.)
    
    d =  int(degrees)
    m = int(minutes)
    s = seconds*60.

    if(np.abs(seconds*60. - sign*60.) <  1e-6):
        s = 0
        m = m + sign
        if m >= 60:
            m = 0
            d = d + sign
            if sign*d >= 360:
                d= d + sign*360
        
    
    return d, m, s

def frac_of_day(time_delta):
    return time_delta.days + time_delta.seconds/calendar.seconds_per_day


def to_julian_centuries(jd):
    return (jd - c.J2000)/c.TO_CENTURIES

def sincos(x):
    return [np.sin(x), np.cos(x)]

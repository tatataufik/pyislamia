"""@package pyislamia.riseset
Collection of functions for calculating moon parameters
"""


import astronomia
from astronomia import dynamical
import numpy as np
from pyislamia import astroOps

_k1 = astronomia.util.d_to_r(360.985647)

class Error(Exception):
    """local exception class"""
    pass


def rise(jd, raList, decList, h0, delta, obs_lon, obs_lat):
    """Return the Julian Day of the rise time of an object.

    Arguments:
      - `jd`     : (int) Julian Day number of the day in question, at 0 hr UT
      - `raList` : (float, float, float) a sequence of three right accension
        values, in radians, for (jd-1, jd, jd+1)
      - `decList`: (float, float, float) a sequence of three right declination
        values, in radians, for (jd-1, jd, jd+1)
      - `h0`     : (float) the standard altitude in radians
      - `delta`  : (float) desired accuracy in days. Times less than one minute
        are infeasible for rise times because of atmospheric refraction.

    Returns:
      - Julian Day of the rise time

    """
    longitude = obs_lon
    latitude = obs_lat
    THETA0 = astronomia.calendar.sidereal_time_greenwich(jd)
    deltaT_days = dynamical.deltaT_seconds(jd) / astronomia.constants.seconds_per_day

    cosH0 = (np.sin(h0) - np.sin(latitude)*np.sin(decList[1])) / (
        np.cos(latitude)*np.cos(decList[1]))
    #
    # future: return some indicator when the object is circumpolar or always
    # below the horizon.
    #
    if cosH0 < -1.0:  # circumpolar
        return None
    if cosH0 > 1.0:  # never rises
        return None

    H0 = np.arccos(cosH0)
    m0 = (raList[1] - longitude - THETA0) / astronomia.constants.pi2
    m = m0 - H0 / astronomia.constants.pi2  # the only difference between rise() and settime()
    if m < 0:
        m += 1
    elif m > 1:
        m -= 1
    if not 0 <= m <= 1:
        raise Error("m is out of range = " + str(m))
    for bailout in range(20):
        m0 = m
        theta0 = astronomia.util.modpi2(THETA0 + _k1 * m)
        n = m + deltaT_days
        if not -1 < n < 1:
            return None  # Bug: this is where we drop some events
        ra = astronomia.util.interpolate_angle3(n, raList)
        dec = astronomia.util.interpolate3(n, decList)
        H = theta0 + longitude - ra
#        if H > pi:
#            H = H - pi2
        H = astronomia.util.diff_angle(0.0, H)
        A, h = astroOps.equ_to_horiz(H, dec, obs_lat)
        dm = (h - h0) / (astronomia.constants.pi2 * np.cos(dec) * np.cos(latitude) * np.sin(H))
        m += dm
        if abs(m - m0) < delta:
            return jd + m

    raise Error("bailout")

def settime(jd, raList, decList, h0, delta, obs_lon, obs_lat):
    """Return the Julian Day of the set time of an object.

    Arguments:
      - `jd`      : Julian Day number of the day in question, at 0 hr UT
      - `raList`  : a sequence of three right accension values, in radians, for
        (jd-1, jd, jd+1)
      - `decList` : a sequence of three right declination values, in radians,
        for (jd-1, jd, jd+1)
      - `h0`      : the standard altitude in radians
      - `delta`   : desired accuracy in days. Times less than one minute are
        infeasible for set times because of atmospheric refraction.

    Returns:
      - Julian Day of the set time

    """
    longitude = obs_lon
    latitude = obs_lat
    THETA0 = astronomia.calendar.sidereal_time_greenwich(jd)
    deltaT_days = dynamical.deltaT_seconds(jd) / astronomia.constants.seconds_per_day

    cosH0 = (np.sin(h0) - np.sin(latitude)*np.sin(decList[1])) / (
        np.cos(latitude)*np.cos(decList[1]))
    #
    # future: return some indicator when the object is circumpolar or always
    # below the horizon.
    #
    if cosH0 < -1.0:  # circumpolar
        return None
    if cosH0 > 1.0:  # never rises
        return None

    H0 = np.arccos(cosH0)
    m0 = (raList[1]  - longitude - THETA0) / astronomia.constants.pi2
    m = m0 + H0 / astronomia.constants.pi2  # the only difference between rise() and settime()
    if m < 0:
        m += 1
    elif m > 1:
        m -= 1
    if not 0 <= m <= 1:
        raise Error("m is out of range = " + str(m))
    for bailout in range(20):
        m0 = m
        theta0 = astronomia.util.modpi2(THETA0 + _k1 * m)
        n = m + deltaT_days
        if not -1 < n < 1:
            return None  # Bug: this is where we drop some events
        ra = astronomia.util.interpolate_angle3(n, raList)
        dec = astronomia.util.interpolate3(n, decList)
        H = theta0 + longitude - ra
#        if H > pi:
#            H = H - pi2
        H = astronomia.util.diff_angle(0.0, H)
        A, h = astroOps.equ_to_horiz(H, dec,obs_lat)
        dm = (h - h0) / (astronomia.constants.pi2 * np.cos(dec) * np.cos(latitude) * np.sin(H))
        m += dm
        if abs(m - m0) < delta:
            return jd + m

    raise Error("bailout")


def transit(jd, raList, delta, obs_lon, obs_lat):
    """Return the Julian Day of the transit time of an object.

    Arguments:
      - `jd`      : Julian Day number of the day in question, at 0 hr UT
      - `raList`  : a sequence of three right accension values, in radians, for
        (jd-1, jd, jd+1)
      - `delta`   : desired accuracy in days.

    Returns:
      - Julian Day of the transit time

    """
   
    longitude = obs_lon
    THETA0 = astronomia.calendar.sidereal_time_greenwich(jd)
    deltaT_days = dynamical.deltaT_seconds(jd) / astronomia.constants.seconds_per_day

    m = (raList[1] - longitude - THETA0) / astronomia.util.pi2
    if m < 0:
        m += 1
    elif m > 1:
        m -= 1
    if not 0 <= m <= 1:
        raise Error("m is out of range = " + str(m))
    for bailout in range(20):
        m0 = m
        theta0 = astronomia.util.modpi2(THETA0 + _k1 * m)
        n = m + deltaT_days
        if not -1 < n < 1:
            return None  # Bug: this is where we drop some events
        ra = astronomia.util.interpolate_angle3(n, raList)
        H = theta0 + longitude - ra
#        if H > pi:
#            H = H - pi2
        H = astronomia.util.diff_angle(0.0, H)
        dm = -H/astronomia.util.pi2
        m += dm
        if abs(m - m0) < delta:
            return jd + m

    raise Error("bailout")



from tool import to_julian_centuries,sincos
from constants import ACS_TO_RAD, MAS_TO_RAD
from astroOps import fundamental_arguments, FUNDAMENTAL_ARGS
from iau2000a_data import iau2000a_lunisolar, iau2000a_planetary


def iau2000a_mean_obliquity(jd):
    """
    * Calculates the mean obliquity of the ecliptic using the IAU 2000 formula.
    *
    * tdb -- TDB to be used for calculations. TT may be used for all but the most
    *        exacting applications.
    *
    * Return: The mean obliquity of the ecliptic in radians.
    """
    t =to_julian_centuries(jd)

    return (84381.406 +
        (-46.836769 +
        (-0.0001831 +
        (0.00200340 +
        (-0.000000576 - 0.0000000434*t) * t) * t) * t) * t) * ACS_TO_RAD



def iau2000a_nutation(jd):
    """
     * Calculates the nutation in longitude and obliquity using the IAU 2000A 
     * nutation model in its entirety.
     *
     * tdb -- TDB to be used for calculations. TT may be used for all but the most
     *        exacting applications.
     * d_psi -- The nutation in longitude expressed in radians.
     * d_epsilon -- The nutation in obliquity expressed in radians.
    """
    t = to_julian_centuries(jd);

    """
     * Get fundamental arguments (planetary longitudes, precession,
     * luni-solar values). We use 1-based indices for phi to make them
     * consistent with the reference document.
    """
    phi = [0.0 for i in range(15)]
    fund_args = fundamental_arguments(t)
    for i in range(0,14):
        phi[i + 1] = fund_args[FUNDAMENTAL_ARGS[i]]

    psi_lun = 0.
    eps_lun = 0.
    """ To maximize precision,sum lunisolar terms with smallest terms first """
    for i in range(len(iau2000a_lunisolar) - 1, -1,-1):
        s_phi = iau2000a_lunisolar[i][0] * phi[10] + \
            iau2000a_lunisolar[i][1] * phi[11] + \
            iau2000a_lunisolar[i][2] * phi[12] + \
            iau2000a_lunisolar[i][3]* phi[13] + \
            iau2000a_lunisolar[i][4] * phi[14]

        [sn,cs] = sincos(s_phi)

        psi_lun += (iau2000a_lunisolar[i][5] + \
            iau2000a_lunisolar[i][6] * t) * sn + \
            iau2000a_lunisolar[i][7] * cs

        eps_lun += (iau2000a_lunisolar[i][8]+ \
            iau2000a_lunisolar[i][9] * t) * cs + \
            iau2000a_lunisolar[i][10] * sn
    

    psi_pla = 0;
    eps_pla = 0;
    """ Sum up planetary terms with the smallest terms first """
    for i in range(len(iau2000a_planetary)-1,-1,-1):
        s_phi = iau2000a_planetary[i][0] * phi[1] + \
            iau2000a_planetary[i][1] * phi[2] + \
            iau2000a_planetary[i][2] * phi[3] + \
            iau2000a_planetary[i][3] * phi[4] + \
            iau2000a_planetary[i][4] * phi[5] + \
            iau2000a_planetary[i][5] * phi[6] + \
            iau2000a_planetary[i][6] * phi[7] + \
            iau2000a_planetary[i][7] * phi[8] + \
            iau2000a_planetary[i][8] * phi[9] + \
            iau2000a_planetary[i][9]* phi[10] + \
            iau2000a_planetary[i][10] * phi[11] + \
            iau2000a_planetary[i][11] * phi[12] + \
            iau2000a_planetary[i][12] * phi[13] + \
            iau2000a_planetary[i][13] * phi[14]

        [sn,cs] = sincos(s_phi)

        psi_pla += iau2000a_planetary[i][14] * sn + \
            iau2000a_planetary[i][15] * cs

        eps_pla += iau2000a_planetary[i][16] * cs + \
            iau2000a_planetary[i][17] * sn
    

    """Sum up the individual contributions """
    d_psi = (psi_lun + psi_pla) * MAS_TO_RAD
    d_epsilon = (eps_lun + eps_pla) * MAS_TO_RAD
    
    return [d_psi, d_epsilon]

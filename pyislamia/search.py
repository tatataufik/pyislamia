"""
@package pyislamia.search
Collection of methods to search for x on targeted  value of function of x, f(x)
"""
import numpy as np
from pyislamia.constants import HOURS_PER_DAY

def refineSearch(jd, fraction, toTarget, maxDiff, obs_lon, obs_lat, tz_offset, targetVal,fn_eval):
    """
    Coarse to fine search function
    """
    delta = 1.;
    iterations = 10;
    while np.abs(delta) > 0.00001 and iterations > 0:
        delta = (-toTarget / maxDiff) / float(HOURS_PER_DAY);
        fraction += delta;
 
        toTarget = fn_eval(jd + fraction, targetVal,obs_lon, obs_lat, tz_offset) - targetVal
        iterations-=1
        
    return fraction
        
def searchMinima(jd, obs_lon, obs_lat, tz_offset, target, x, y, fn_eval):
    """
    Function to search minima using three points, where x1<x2<x3 and f(x1)>f(x2) and f(x2) < f(x3) 
    """
    iterations = 20
    flag_break = False
    while not flag_break:
        fraction= 0.5*(x[0]+x[2])
        if fraction == x[1]:
            fraction = fraction + 1e-3 if y[2] < y[0] else fraction - 1e-3;
        ymid = fn_eval(jd + fraction, target, obs_lon,obs_lat, tz_offset);
        
        if fraction > x[1]:
            if ymid < y[1]:
                x[0] = x[1]
                y[0] = y[1]
                
                x[1] = fraction
                y[1] = ymid
            else:
                x[2] =  fraction
                y[2] = ymid
        else:
            if ymid < y[1]:
                x[2] = x[1]
                y[2] = y[1]
                x[1] = fraction
                y[1] = ymid
            else:
                x[0] = fraction
                y[0] = ymid
            
        flag_break = (x[2] - x[0] <= 1e-5 or iterations <= 0)
        iterations -= 1
    return x[1]

def searchMaxima(jd, obs_lon, obs_lat, tz_offset, target, x, y, fn_eval):
    """
    Function to search maxima using three points, where x1<x2<x3 and f(x1) < f(x2) and f(x2) > f(x3) 
    """
    iterations = 20;
    flag_break = False
    while not flag_break:
        fraction = 0.5 * (x[0] + x[2])
        if fraction == x[1]:
            fraction = fraction + 1e-3 if y[2]>y[0] else fraction - 1e-3
            
        ymid = fn_eval(jd+fraction, target, obs_lon, obs_lat, tz_offset)
        if fraction > x[1]:
            if ymid > y[1]:
                x[0] = x[1]
                y[0] = y[1]
                x[1] = fraction
                y[1] = ymid
            else:
                x[2] = fraction
                y[2] = ymid
                
        else:
            if ymid > y[1]:
                x[2] = x[1]
                y[2] = y[1]
                x[1] = fraction
                y[1] = ymid
                
            else:
                x[0] = fraction
                y[0] = ymid
                
        flag_break = (x[2] - x[0] <= 1e-5 or iterations <= 0)
        iterations -= 1
    
    return x[1]

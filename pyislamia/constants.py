"""@package pyislamia.constants
Collection global constant values.
"""
from astronomia import util
 
##
# @cvar Average moon age in a lunar month.
#
SYNODIC_MONTH = 29.530588861

##
# @cvar Number of hours per day.
#
HOURS_PER_DAY = 24.0


##
# @cvar SUN altitude on rising or setting.
#
SUN_ALT = util.d_to_r(-0.83333)

##
# @cvar MOON altitude on rising or setting.
#
MOON_ALT = util.d_to_r(0.125)

##
# @cvar Standard of SUN altitude on rising or setting for CIVIL TWILIGHT.
#
C_TWI_ALT = util.d_to_r(-6.0)

##
# @cvar Standard of SUN altitude on rising or setting for NAUTICAL TWILIGHT
#
N_TWI_ALT = util.d_to_r(-12.0)

##
# @cvar Standard of SUN altitude on rising or setting for ASTRONOMICAL TWILIGHT
#
A_TWI_ALT = util.d_to_r(-18.0)

##
# @cvar 1 AU in kilometers.
#
AU = 149597870.691

##
# @cvar KA'BAH geographic LATITUDE.
#
KAABAH_LAT = util.d_to_r(21.42248)

##
#@cvar KA'BAH geographic LONGITUDE.
#
KAABAH_LON = util.d_to_r(39.82617)

##
#@cvar number days in a century
#
TO_CENTURIES = 36525.0

##
#@cvar J2000 reference
#
J2000 = 2451545.0


EARTH_FLATTENING = (1.0 / 298.25642)
##
#@cvar Earth's equatorial radius(meters)
#
EARTH_EQU_RADIUS = 6378136.6 
##
#@cvar
#
EARTH_POL_RADIUS  = (EARTH_EQU_RADIUS * (1.0 - EARTH_FLATTENING))

##
#@cvar arcsecond to radian
#
ACS_TO_RAD = 0.000004848136811095359935899141

##
#@cvar milliarcsecond to radian
#
MAS_TO_RAD =0.000000004848136811095359935899141
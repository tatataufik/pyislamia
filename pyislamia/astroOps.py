"""@package pyislamia.astroOps
Collection of functions for calculating solar parameters
"""
from pyislamia.tool import start_of_day, to_julian_centuries
from astronomia.calendar import fday_to_hms
from astronomia.util import d_to_r

"""
@mainpage PyIslamia
@author Tata Taufik Nugraha
<hr/>
@section Introduction
PyIslamia is python library to calculate various aspects need by muslim based on astronomical algorithm.
"""

import numpy as np
from astronomia import util,calendar, nutation
from constants import ACS_TO_RAD

FUNDAMENTAL_ARGS = {0: "ARG_LONGITUDE_MERCURY",
        1: "ARG_LONGITUDE_VENUS",
        2: "ARG_LONGITUDE_EARTH",
        3: "ARG_LONGITUDE_MARS",
        4: "ARG_LONGITUDE_JUPITER",
        5: "ARG_LONGITUDE_SATURN",
        6: "ARG_LONGITUDE_URANUS",
        7: "ARG_LONGITUDE_NEPTUNE",
        8: "ARG_PRECESSION",
        9: "ARG_ANOMALY_MOON",
        10: "ARG_ANOMALY_SUN",
        11: "ARG_LATITUDE_MOON",
        12: "ARG_ELONGATION_MOON",
       13: "ARG_LONGITUDE_NODE",
       14: "ARG_LONGITUDE_MOON",
       }

def equ_to_horiz(H, decl, obs_latitude):
    """Convert equitorial to horizontal coordinates.

    [Meeus-1998: equations 12.5, 12.6]

    Note that azimuth is measured westward starting from the south.

    This is not a good formula for using near the poles.

    Arguments:
      - `H` : hour angle in radians
      - `decl` : declination in radians

    Returns:
      - azimuth in radians
      - altitude in radians

    """
    cosH = np.cos(H)
    sinLat = np.sin(obs_latitude)
    cosLat = np.cos(obs_latitude)
    A = np.arctan2(np.sin(H), cosH * sinLat - np.tan(decl) * cosLat)
    h = np.arcsin(sinLat * np.sin(decl) + cosLat * np.cos(decl) * cosH)
    
    return A, h

def apparent_greenwich_siderial_time(jd):
    """
    """
   
    mean_greenwich_sidereal_time  = calendar.sidereal_time_greenwich(jd)

    nutInLongitude = nutation.nutation_in_longitude(jd)
    obliquity = nutation.obliquity(jd)
    nutInObliquity = nutation.nutation_in_obliquity(jd)
    obliquity += nutInObliquity
    
    return util.modpi2( mean_greenwich_sidereal_time + nutInLongitude*np.cos(obliquity))
    

def calc_hour_angle(jd, obs_lon, ra):
    """
    @brief Calculate hour angle (equatorial coordinate) given julian_day, observer longitude and the right ascension)
    @param jd julian day
    @param obs_lon observer's longitude in radian (negative value for west and positive value for east
    @param ra the right ascension value in equatorial coordinate
    @return the hour angle value for horizontal coordinate calculation
    """
    
    apparent_gmst =  apparent_greenwich_siderial_time(jd)
   
    local_siderial = apparent_gmst + obs_lon  #local sidereal time at sun set
    '''hour angle of the moon'''
    H = util.modpi2(local_siderial - ra)
   
    return H

def calc_elongation(ra1, dec1, ra2, dec2):
    """
    @brief Calculate elongation.
    @param ra1 right ascension of first object.
    @param dec1 declination angle of first object.
    @param ra2 the right ascension value of second object.
    @param dec2 declination angle of second object.
    @return the elongation angle in its reference.
    """
    return np.arccos(np.sin(dec1) * np.sin(dec2) + np.cos(dec1) * np.cos(dec2) * np.cos(ra1 - ra2))



def fundamental_argument(arg, t):
    """
     Calculates values for the various fundamental arguments used in the planetary,
     lunar, precession and nutation models.
    
     arg -- One of the values from the fund_argument enumeration.
     t -- Number of Julian centuries of TDB since 2000-01-01 12h TDB. TT may be used
          for all but the most exacting applications.
 
     Return: Nonee if the arg parameter is invalid. The value of the fundamental
          argument in radians otherwise.
    """
    fund_args ={
        "ARG_LONGITUDE_MERCURY": 4.402608842 + 2608.7903141574 * t,
        "ARG_LONGITUDE_VENUS": 3.176146697 + 1021.3285546211 * t,
        "ARG_LONGITUDE_EARTH": 1.753470314 + 628.3075849991 * t,
        "ARG_LONGITUDE_MARS": 6.203480913 + 334.0612426700 * t,
        "ARG_LONGITUDE_JUPITER": 0.599546497 + 52.9690962641 * t,
        "ARG_LONGITUDE_SATURN": 0.874016757 + 21.3299104960 * t,
        "ARG_LONGITUDE_URANUS": 5.481293872 + 7.4781598567 * t,
        "ARG_LONGITUDE_NEPTUNE": 5.311886287 + 3.8133035638 * t,
        "ARG_PRECESSION": (0.024381750 + 0.00000538691 * t) * t,
        "ARG_ANOMALY_MOON": (485868.249036 +
            (1717915923.2178 +
            (31.8792 +
            (0.051635 - 0.00024470 * t) * t) * t) * t) * ACS_TO_RAD,
        "ARG_ANOMALY_SUN": (1287104.79305 +
            (129596581.0481 +
            (-0.5532 +
            (0.000136 - 0.00001149 * t) * t) * t) * t) * ACS_TO_RAD,
        "ARG_LATITUDE_MOON": (335779.526232 +
            (1739527262.8478 +
            (-12.7512 +
            (-0.001037 + 0.00000417 * t) * t) * t) * t) * ACS_TO_RAD,
        "ARG_ELONGATION_MOON": (1072260.70369 +
            (1602961601.2090 +
            (-6.3706 +
            (0.006593 - 0.00003169 * t) * t) * t) * t) * ACS_TO_RAD,
       "ARG_LONGITUDE_NODE": (450160.398036 +
            (-6962890.5431 +
            (7.4722 +
            (0.007702 - 0.00005939 * t) * t) * t) * t) * ACS_TO_RAD,
       "ARG_LONGITUDE_MOON": (785939.95571 +
            (1732559343.73604 +
            (-5.8883 +
            (0.006604 - 0.00003169 * t) * t) * t) * t) * ACS_TO_RAD
    }
   
    return fund_args[arg]

def fundamental_arguments(t):
    """
     Calculates values for the various fundamental arguments used in the planetary,
     lunar, precession and nutation models.
    
     arg -- One of the values from the fund_argument enumeration.
     t -- Number of Julian centuries of TDB since 2000-01-01 12h TDB. TT may be used
          for all but the most exacting applications.
 
     Return: Nonee if the arg parameter is invalid. The value of the fundamental
          argument in radians otherwise.
    """
    fund_args ={
        "ARG_LONGITUDE_MERCURY": 4.402608842 + 2608.7903141574 * t,
        "ARG_LONGITUDE_VENUS": 3.176146697 + 1021.3285546211 * t,
        "ARG_LONGITUDE_EARTH": 1.753470314 + 628.3075849991 * t,
        "ARG_LONGITUDE_MARS": 6.203480913 + 334.0612426700 * t,
        "ARG_LONGITUDE_JUPITER": 0.599546497 + 52.9690962641 * t,
        "ARG_LONGITUDE_SATURN": 0.874016757 + 21.3299104960 * t,
        "ARG_LONGITUDE_URANUS": 5.481293872 + 7.4781598567 * t,
        "ARG_LONGITUDE_NEPTUNE": 5.311886287 + 3.8133035638 * t,
        "ARG_PRECESSION": (0.024381750 + 0.00000538691 * t) * t,
        "ARG_ANOMALY_MOON": (485868.249036 +
            (1717915923.2178 +
            (31.8792 +
            (0.051635 - 0.00024470 * t) * t) * t) * t) * ACS_TO_RAD,
        "ARG_ANOMALY_SUN": (1287104.79305 +
            (129596581.0481 +
            (-0.5532 +
            (0.000136 - 0.00001149 * t) * t) * t) * t) * ACS_TO_RAD,
        "ARG_LATITUDE_MOON": (335779.526232 +
            (1739527262.8478 +
            (-12.7512 +
            (-0.001037 + 0.00000417 * t) * t) * t) * t) * ACS_TO_RAD,
        "ARG_ELONGATION_MOON": (1072260.70369 +
            (1602961601.2090 +
            (-6.3706 +
            (0.006593 - 0.00003169 * t) * t) * t) * t) * ACS_TO_RAD,
       "ARG_LONGITUDE_NODE": (450160.398036 +
            (-6962890.5431 +
            (7.4722 +
            (0.007702 - 0.00005939 * t) * t) * t) * t) * ACS_TO_RAD,
       "ARG_LONGITUDE_MOON": (785939.95571 +
            (1732559343.73604 +
            (-5.8883 +
            (0.006604 - 0.00003169 * t) * t) * t) * t) * ACS_TO_RAD
    }
   
    return fund_args